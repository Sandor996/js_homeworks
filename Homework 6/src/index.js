//Задание 1. Разработайте функцию-конструктор, которая будет создавать объект Human (человек). Создайте массив объектов и реализуйте функцию которая будет сортировать элементы массива по значению свойства Age по возрастанию или убыванию.
function Human(name, surname, age) {           // Создаем функцию-конструктор
    this.name = name;
    this.surname = surname;
    this.age = age;
}

let people = new Array();                               // Создаем массив людей
people[0] = new Human("Petro", "Pyatochkin", 18);
people[1] = new Human("Dmytro", "Shkurka", 25);
people[2] = new Human("Oleksiy", "Lutsenko", 23);
people[4] = new Human("Roman", "Kostenko", 24);


function sortingFunction(array) {                       // Создаем функцию сортировки
    array.sort(function (a, b) {
        return a.age > b.age ? 1 : -1
    });
}

function show(array) {                                   // Создаем функцию вывода
    array.forEach(function (value, index, arr) {

        document.write(`Name - ${array[index].name}, surname - ${array[index].surname}, age - "${array[index].age}" years<br> `)
    })

}

sortingFunction(people);                                // Вызов функции сортировки массива
show(people);                                           // Вызов функции вывода

document.write(`<hr>`)
// Задание 2. Разработайте функцию-конструктор, которая будет создавать объект Human (человек), добавьте на своё усмотрение свойства и методы в этот объект. Подумайте, какие методы и свойства следует сделать уровня экзепляра, а какие уровня функции-конструктора.

function Human2(name, surname, age) {                   // Создаем функцию-конструктор
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.showInfo= function(){
        document.write(`${this.name} ${this.surname} is ${this.age} years old<br>`) // Метод уровня экземпляра
    }
}
Human2.allowed = function () {                // Метод уровня функции-конструктора
    return this.age >= 18 ? document.write(`${this.name} ${this.surname} - is allowed to drive car`) : document.write(`${this.name} ${this.surname} - is not allowed to drive car`);
}

const yourName = prompt("Please enter your name", "Vasya");
const yourSurname = prompt("Please enter your surname", "Cabina");
const yourAge = +prompt("Please enter your age", "30");

const yourProfile = new Human2(yourName, yourSurname, yourAge); // Создание объекта с помощью функции конструктора

yourProfile.showInfo();                                 // Вызов метода уровня экземпляра

yourProfile.allowed();                                  // Вызов метода уровня функции-конструктора
