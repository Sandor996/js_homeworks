let styles = ["jazz", "blues"]; // Исходный массив
document.write("Исходный массив <b>styles : </b>" + styles.join(", ") + ".<br><br>");

styles.push("rock-n-roll"); // добавляем новый элемент в массив
document.write("массив с новым элементом в конце <b>styles : </b>" + styles.join(", ") + ".<br><br>");

let middle = Math.round(styles.length / 2); // формула для нахождения лемента по середине
styles.splice(middle - 1, 1, "classic"); // заменяем элемент по середине
document.write("массив с измененным элементом по середине <b>styles : </b>" + styles.join(", ") + ".<br><br>");

styles.unshift("rap", "reggae"); // добавляем в начало массива новые элементы
document.write("массив с новыми элементами в начале <b>styles : </b>" + styles.join(", ") + ".<br><br>");