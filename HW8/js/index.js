/* Нарисовать на странице круг используя параметры, которые введет пользователь.
При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".
Данная кнопка должна являться единственным контентом в теле HTML документа,
весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга.
При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета.
При клике на конкретный круг - этот круг должен исчезать,
при этом пустое место заполняться, то есть все остальные круги сдвигаются влево. */

window.onload = () => {
    const diameterInput = document.createElement("input"); // Создаем поле для ввода диаметра
    diameterInput.setAttribute("type", "number");
    diameterInput.setAttribute("placeholder", "Ввести диаметр круга, мм");
    diameterInput.style.marginLeft = "50px";

    const drawButton = document.createElement("input");    // создаем кнопку Нарисовать
    drawButton.setAttribute("type", "button");          
    drawButton.setAttribute("value", "Нарисовать");
    drawButton.setAttribute("id", "draw");
    drawButton.style.cursor = "pointer";
    drawButton.style.marginLeft = "10px";

    const drawCircleButton = document.querySelector("#drawCircle");  // ищем кнопку Нарисовать круг в HTML;
    drawCircleButton.style.cursor = "pointer";

    drawCircleButton.addEventListener("click", () => {              // добавляем кнопку Нарисовать и поле для ввода диаметра 
        document.body.append(diameterInput);
        document.body.append(drawButton);
    })

    const divСontainer = document.createElement("div");                       // создаем переменную с контейнером для кружков
    divСontainer.setAttribute("class", "container");
    divСontainer.style.display = "flex";
    divСontainer.style.justifyContent = "space-between";
    divСontainer.style.flexWrap = "wrap";
    divСontainer.style.marginTop = "20px";

    function getRandomColor() {                                             // функция случайного цвета
        let r = function () { return Math.floor(Math.random() * 256) };
        return `rgb(${r()}, ${r()}, ${r()})`;
    }

    drawButton.addEventListener("click", () => {
        document.body.append(divСontainer);                                               // добавляем контейнер для кружков на страницу
        if (diameterInput.value > 0) {                                                    // создаем условие при котором будут добавляться кружки
            for (let i = 0; i < 100; i++) {                                               // создаем цикл для добавления 100 кружков разного цвета
                const divCircle = document.createElement("div");                          // создаем переменную для кружков
                divCircle.setAttribute("class", "circle");
                divCircle.style.width = diameterInput.value + "px";
                divCircle.style.height = diameterInput.value + "px";
                divCircle.style.borderRadius = "50%";
                divCircle.style.backgroundColor = getRandomColor();
                divCircle.style.cursor = "pointer";
                divCircle.addEventListener("click", (circle) => {                          // добавляем возможность удаления для каждого кружка
                    circle.target.remove();
                })
                divСontainer.append(divCircle);                                            // добавляем кружок
            }
        } else alert("Введите число больше нуля");
    })

}













