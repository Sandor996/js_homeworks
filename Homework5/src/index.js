//Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.

let documentObj = {   
    heading: "",
    body: "",
    footer: "",
    data: "",
    app: {
        heading: {},
        body: {},
        footer: {},
        data: {}
    },
    writeIn: function () {
        this.heading = prompt("Write in heading", "heading");
        this.body = prompt("Write in body", "body");
        this.footer = prompt("Write in footer", "footer");
        this.data = prompt("Write in data", "data");
        this.app.heading = prompt("Write in application heading", "application heading");
        this.app.body = prompt("Write in application body", "application body");
        this.app.footer = prompt("Write in application footer", "application footer");
        this.app.data = prompt("Write in application data", "application data");
    },
    show: function(){
        document.write(`Main object - "documentObj":<br>`);
        document.write(`Main object heading - "${this.heading}"<br>`);
        document.write(`Main object body - "${this.body}"<br>`);
        document.write(`Main object footer - "${this.footer}"<br>`);
        document.write(`Main object data - "${this.data}"<br>`);
        document.write(`<br><br>`)
        document.write(`Nested object - "app":<br>`);
        document.write(`Nested object heading - "${this.app.heading}"<br>`);
        document.write(`Nested object body - "${this.app.body}"<br>`);
        document.write(`Nested object footer - "${this.app.footer}"<br>`);
        document.write(`Nested object data - "${this.app.data}"<br>`);
    }
};
documentObj.writeIn();
documentObj.show();