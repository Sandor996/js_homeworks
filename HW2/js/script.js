//a + b < 4         Задание 1
document.write("a + b < 4<br><br>")
let a = parseInt(prompt("Введите число a", "0"));
let b = parseInt(prompt("Введите число b", "0"));
let result = a + b;
result < 4 ? document.write(result + " мало") : document.write(result + " много");

//Задание 2

let message;
let login = prompt("Введите Ваше имя", "Имя");
message = login == "Вася" ? "Привет " : login == "Директор " ? "Здравствуйте " : login == "" ? "Нет логина" : "Доброго времени суток " + login;
document.write("<br><br>" + message + "<br><br>");

//Задание 3
//A<B вывести сумму и все нечетные числа в промежутке от А до В
let A = parseInt(prompt("A < B Введите А", "1"));
let B = parseInt(prompt("A < B Введите B", "100"));
let sum = A + B;

document.write("Число А = " + A + "; " + "Число В = " + B + "<br>");

for (let i = A; i < B; i++) {
    if (i % 2 != 0) {
        document.write(i);
    }
    document.write(" ")
}
document.write("<br><br>");

//Прямоугольник
for (let i = 0; i < 10; i++) { //Задаем высоту прямоугольника,делая переносы строк
    for (let j = 0; j < 40; j++) {
        document.write("&#10031;"); //Задаем ширину прямоугольника
    }
    document.write("<br>");
}
document.write("Прямоугольник" + "<br><br>");

//Прямоугольный треугольник
for (let i = 0; i < 15; i++) { //Задаем высоту треугольника
    for (j = 0; j < i; j++) { //Задаем длину основания треугольника
        document.write("&#8710;");
    }
    document.write("<br>");
}
document.write("Прямоугольный треугольник" + "<br><br>");

//Равносторонний треугольник

let triangle = 16;
for (let i = 0; i < triangle; i++) { //Задаем высоту треугольника,ставя разрывы строк
    for (j = i; j < triangle; j++) { // Внутренний цикл, ставим пробелы
        document.write("&nbsp;");
    }
    for (let k = i; k >= 0; k--) {// Внутренний цикл, вставляем внутрь символы
        document.write("&#8710;");
    }

    document.write("<br>");
}
document.write("Равносторонний треугольник" + "<br><br>");

// Ромб
let rhombus = 10;
for (let i = 0; i < rhombus; i++) {//Задаем высоту половинки ромба,ставя разрывы строк
    for (let j = i; j < rhombus; j++) {// Внутренний цикл, ставим пробелы
        document.write("&nbsp;");
    }
    for (let k = i; k >= 0; k--) {// Внутренний цикл, вставляем внутрь символы
        document.write("&#9830;");
    }
    document.write("<br>");
}
for (let g = 0; g < rhombus; g++) {//Задаем высоту половинки ромба,ставя разрывы строк
    for (let h = g; h >= 0; h--) {// Внутренний цикл, ставим пробелы
        document.write("&nbsp;");
    }
    for (let l = g; l < rhombus; l++) {// Внутренний цикл, вставляем внутрь символы
        document.write("&#9830;");
    }
    document.write("<br>");
}
document.write("Ромб" + "<br><br>");
